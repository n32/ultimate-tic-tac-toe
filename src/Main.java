import java.util.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    public static void main(String args[]) {
        Random random = new Random();
        Scanner in = new Scanner(System.in);

        // game loop
        while (true) {
            int opponentRow = in.nextInt();
            int opponentCol = in.nextInt();
            int validActionCount = in.nextInt();
            List<String> validActions = new ArrayList<>(validActionCount);
            for (int i = 0; i < validActionCount; i++) {
                int row = in.nextInt();
                int col = in.nextInt();

                validActions.add(String.format("%s %s", row, col));
            }

            // Write an action using System.out.println()
            // To debug: System.err.println("Debug messages...");

            String move = validActions.get(random.nextInt(validActions.size()));
            System.out.println(move);
        }
    }
}